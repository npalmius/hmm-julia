# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 22-Sep-2014

using HMM
using Distributions
using MLBase
using Plotly

include("..\\plotly-julia\\plotly_signin.jl");

function hmm_forward_backward(data)
    T = length(data.y);

    S = length(data.θ.o);
    S_VALID = find(data.θ.o);

    O_Δ = 0.001;
    
    multivariate = false;

    data_dims = 1;
    mv_dim = 0;
    mv_dist = nothing;

    if typeof(data.θ.o[1]) <: UnivariateDistribution
    elseif typeof(data.θ.o[1]) <: MultivariateDistribution
        T = size(data.y, 2);
        
        data_dims = size(data.y, 1);
        multivariate = true;

        first_dim = 0
        all_diag = true;

        for s = S_VALID
            if first_dim == 0
                first_dim = data.θ.o[s].dim;
                @assert (size(data.y, 1) == first_dim) "Multivariate observation distribution dimensionality does not correspond with data dimensionality."
            else
                @assert (data.θ.o[s].dim == first_dim) "All multivariate observation distribution must have the same number of dimensions."
            end
            full_Σ = full(data.θ.o[s].Σ);
            all_diag = all_diag && (full_Σ .* eye(first_dim) == full_Σ); # "Only diagonal multivariate normal distributions are currently supported."
        end
        if all_diag
            mv_dim = first_dim;
            mv_dist = cell(S, first_dim);
            for s = S_VALID
                full_Σ = full(data.θ.o[s].Σ);
                for d = 1:first_dim
                    mv_μ = data.θ.o[s].μ[d];
                    mv_Σ = full_Σ[d, d];
                    mv_dist[s, d] = Normal(mv_μ, sqrt(mv_Σ));
                end
            end
        end
    else
        @assert false "Invalid observation distribution"
    end

    α = zeros(T, S);
    α_z = zeros(T, 1);

    for t = 1:T
        for s = S_VALID
            if t == 1
                α[t, s] = Distributions.pdf(data.θ.π, s);
            else
                for s_prev = S_VALID
                    α[t, s] += α[t - 1, s_prev] * Distributions.pdf(data.θ.a[s_prev], s);
                end
            end
            
            p_observation = 1;

            # Probability of observation given state s
            if multivariate
                if mv_dim > 0
                    for d = 1:mv_dim
                        if O_Δ > 0
                            p_observation *= ((Distributions.cdf(mv_dist[s, d], data.y[d, t] + O_Δ) - Distributions.cdf(mv_dist[s, d], data.y[d, t] - O_Δ)) / (2 * O_Δ));
                        else
                            p_observation *= Distributions.pdf(mv_dist[s, d], data.y[d, t]);
                        end
                    end
                else
                    p_observation = Distributions.pdf(data.θ.o[s], data.y[:, t]);
                end
            else
                if O_Δ > 0
                    p_observation = ((Distributions.cdf(data.θ.o[s], data.y[t] + O_Δ) - Distributions.cdf(data.θ.o[s], data.y[t] - O_Δ)) / (2 * O_Δ));
                else
                    p_observation = Distributions.pdf(data.θ.o[s], data.y[t]);
                end
            end

            α[t, s] *= p_observation;
        end
        
        α_z[t] = sum(α[t, :]);
        α[t, :] /= α_z[t];
    end

    p_observed_log = sum(log(α_z));

    β = zeros(T, S);
    β_z = zeros(T, 1);

    for t = T:-1:1
        for s = S_VALID
            if t == T
                β[t, s] = 1;
            else
                for s_next = S_VALID
                    β[t, s] += β[t + 1, s_next] * Distributions.pdf(data.θ.a[s], s_next);

                    p_observation = 1;

                    # Probability of observation given state s_next
                    if multivariate
                        if mv_dim > 0
                            for d = 1:mv_dim
                                if O_Δ > 0
                                    p_observation *= ((Distributions.cdf(mv_dist[s_next, d], data.y[d, t + 1] + O_Δ) - Distributions.cdf(mv_dist[s_next, d], data.y[d, t + 1] - O_Δ)) / (2 * O_Δ));
                                else
                                    p_observation *= Distributions.pdf(mv_dist[s_next, d], data.y[d, t + 1]);
                                end
                            end
                        else
                            p_observation = Distributions.pdf(data.θ.o[s_next], data.y[:, t + 1]);
                        end
                    else
                        if O_Δ > 0
                            p_observation = ((Distributions.cdf(data.θ.o[s_next], data.y[t + 1] + O_Δ) - Distributions.cdf(data.θ.o[s_next], data.y[t + 1] - O_Δ)) / (2 * O_Δ));
                        else
                            p_observation = Distributions.pdf(data.θ.o[s_next], data.y[t + 1]);
                        end
                    end

                    β[t, s] *= p_observation;
                end
                
            end
        end
        β_z[t] = sum(β[t, :]);
        β[t, :] /= β_z[t];
    end

    μ = α .* β;

    forward_path = zeros(1, T);
    forward_backward_path = zeros(1, T);

    for t = 1:T
        forward_path[t] = findmax(α[t,:])[2]
        forward_backward_path[t] = findmax(μ[t,:])[2]
    end

    state1 = [
      "x" => data.x,
      "y" => forward_path,
      "mode" => "lines",
      "line" => ["shape" => "hvh"],
      "type" => "scatter",
      "name" => "Forward Path"
    ];

    state2 = [
      "x" => data.x,
      "y" => forward_backward_path,
      "mode" => "lines",
      "line" => ["shape" => "hvh"],
      "type" => "scatter",
      "name" => "Forward Backward Path"
    ];

    plot_data = [state1, state2]

    for i = 1:data_dims
        if data_dims == 1
            name_str = "Observed data";
            y = data.y;
        else
            name_str = string("Observed data ", i);
            y = data.y[i, :];
        end

        observed = [
          "x" => data.x,
          "y" => y,
          "mode" => "lines",
          "line" => ["shape" => "hvh"],
          "type" => "scatter",
          "name" => name_str
        ];

        plot_data = [plot_data, observed];
    end

    response = Plotly.plot([plot_data], ["filename" => "hmm-forward-backward", "fileopt" => "overwrite"]);
    plot_url = response["url"];
    print("Plotting inferred data : ");
    println(plot_url);

    println(string("Forward percent correct: ", correctrate(int64(data.z), int64(vec(forward_path)))));
    println(string("Forward backward percent correct: ", correctrate(int64(data.z), int64(vec(forward_backward_path)))));

    return forward_backward_path;
end
