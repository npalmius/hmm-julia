# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 22-Sep-2014

using HMM
using Plotly

include("..\\plotly-julia\\plotly_signin.jl");

function hmm_generative(θ)
    return hmm_generative(θ, Dict());
end

function hmm_generative(θ, opts)
    # Set default options
    T = 100;

    # Process options
    if haskey(opts, "T")
        T = opts["T"];
    end

    data = HMM_Data();

    x = [1:T];

    z = zeros(size(x));

    multivariate = false;

    if typeof(θ.o[1]) <: UnivariateDistribution
        y = zeros(size(x));
    elseif typeof(θ.o[1]) <: MultivariateDistribution
        y = zeros(length(x), length(θ.o[1]));
        multivariate = true;
    else
        @assert false "Invalid observation distribution"
    end

    current_state = 0;

    for i = 1:length(x)
        if i == 1
            current_state = rand(θ.π);
        else
            current_state = rand(θ.a[current_state]);
        end

        z[i] = current_state;

        y[i, :] = rand(θ.o[current_state]);
    end

    data.θ = θ;
    data.x = x;
    if multivariate
        data.y = y';
    else
        data.y = y;
    end
    data.z = z;

    state = [
      "x" => x,
      "y" => z,
      "mode" => "lines",
      "line" => ["shape" => "hvh"],
      "type" => "scatter",
      "name" => "State"
    ];

    plot_data = [state]

    for i = 1:size(y,2)
        if size(y,2) == 1
            name_str = "Observed data"
        else
            name_str = string("Observed data ", i)
        end

        observed = [
          "x" => x,
          "y" => y[:, i],
          "mode" => "lines",
          "line" => ["shape" => "hvh"],
          "type" => "scatter",
          "name" => name_str
        ];

        plot_data = [plot_data, observed];
    end

    response = Plotly.plot([plot_data], ["filename" => "hmm-generative", "fileopt" => "overwrite"]);
    plot_url = response["url"];
    print("Plotting generated data : ");
    println(plot_url);

    return data;
end
