#################################################

cd("C:\\Research\\git\\hmm-julia")

#################################################

using HMM
using Distributions

θ_uv=hmm_init()
θ_mv=hmm_init_mv()

include("hmm_generative.jl")

data_uv=hmm_generative(θ_uv)
data_mv=hmm_generative(θ_mv)

include("hmm_forward_backward.jl")

result_fb_uv = hmm_forward_backward(data_uv);
result_fb_mv = hmm_forward_backward(data_mv);

include("hmm_viterbi.jl")

result_viterbi_uv = hmm_viterbi(data_uv);
result_viterbi_mv = hmm_viterbi(data_mv);

confusmat(length(data_uv.θ.o), int64(data_uv.z), int64(vec(result_fb_uv)))
confusmat(length(data_mv.θ.o), int64(data_mv.z), int64(vec(result_fb_mv)))

confusmat(length(data_uv.θ.o), int64(data_uv.z), int64(vec(result_viterbi_uv)))
confusmat(length(data_mv.θ.o), int64(data_mv.z), int64(vec(result_viterbi_mv)))
